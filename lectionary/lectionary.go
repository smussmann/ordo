package lectionary

//   Copyright 2016 Sam Mussmann
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import (
	"errors"
	"html/template"
	"time"
)

type Lectionary interface {
	GetReading(time.Time) string
}

var LectionaryMap = template.FuncMap{
	"weekly_lectionary":         NewWeeklyLectionary,
	"daily_lectionary":          NewDailyLectionary,
	"monthly_lectionary":        NewMonthlyLectionary,
	"week_at_a_time_lectionary": NewWeekAtATime,
	"pillar_lectionary":         NewPillarLectionary,
}

func NewWeeklyLectionary(readings ...string) (*WeeklyLectionary, error) {
	if len(readings) != 7 {
		return nil, errors.New("expected 7 readings.")
	}
	return &WeeklyLectionary{readings}, nil
}

type WeeklyLectionary struct {
	readings []string
}

func (l *WeeklyLectionary) GetReading(t time.Time) string {
	return l.readings[t.Weekday()]
}

func NewDailyLectionary(reading string) (*DailyLectionary, error) {
	return &DailyLectionary{reading}, nil
}

type DailyLectionary struct {
	reading string
}

func (l *DailyLectionary) GetReading(t time.Time) string {
	return l.reading
}

func NewMonthlyLectionary(readings ...string) (*MonthlyLectionary, error) {
	if len(readings) != 31 {
		return nil, errors.New("expected 31 readings.")
	}
	return &MonthlyLectionary{readings}, nil
}

type MonthlyLectionary struct {
	readings []string
}

func (l *MonthlyLectionary) GetReading(t time.Time) string {
	return l.readings[t.Day()]
}

func NewWeekAtATime(readings ...string) (*weekAtATimeLectionary, error) {
	return &weekAtATimeLectionary{readings}, nil
}

type weekAtATimeLectionary struct {
	readings []string
}

func (l *weekAtATimeLectionary) GetReading(t time.Time) string {
	year, week := t.ISOWeek()
	week_index := int(float64(year)*52.1775) + week
	return l.readings[week_index%len(l.readings)]
}
