package lectionary_test

//   Copyright 2016 Sam Mussmann
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import (
	"testing"
	"time"

	"bitbucket.org/smussmann/ordo/lectionary"
)

func makeDate(year int, month time.Month, day int) time.Time {
	return time.Date(year, month, day, 0, 0, 0, 0, time.Local)
}

func TestDates(t *testing.T) {
	l := lectionary.NewPillarLectionary()

	table := []struct {
		year    int
		month   time.Month
		day     int
		reading string
	}{
		{2016, 10, 24, "Nehemiah 2:11-20; John 10:1-13"},
		{2016, 11, 28, "Malachi 3:1-6; Matthew 3:1-6"},
		{2016, 12, 16, "Isaiah 2:1-5; Acts 11:1-18"},
		{2016, 12, 17, "Proverbs 8:22-31; 1 Corinthians 2:1-13"},
		{2016, 12, 31, "Ecclesiastes 3:1-13; Revelation 21:1-8"},
		{2017, 1, 2, "Isaiah 66:6-14; Matthew 12:46-50"},
		{2017, 1, 5, "Isaiah 12; 2 Corinthians 2:12-end"},
		{2017, 1, 7, "Genesis 25:19-end; Ephesians 1:1-6"},
		{2017, 1, 9, "Isaiah 41:14-20; John 1:29-34"},
		{2017, 2, 2, "Isaiah 52:1-12; Matthew 10:1-15"},
		{2017, 2, 3, "2 Samuel 11:1-17; Matthew 14:1-12"},
		{2017, 4, 17, "Isaiah 54:1-14; Romans 1:1-7"},
		{2017, 5, 24, "Isaiah 43:1-13; Titus 2:11-3:8"},
	}

	for _, entry := range table {
		d := makeDate(entry.year, entry.month, entry.day)
		if l.GetReading(d) != entry.reading {
			t.Errorf("l.GetReading(%s) got %q (want %q)", d, l.GetReading(d), entry.reading)
		}
	}
}
