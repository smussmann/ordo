package ordo

//   Copyright 2016 Sam Mussmann
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import (
	"bitbucket.org/smussmann/ordo/antiphons"
	"bitbucket.org/smussmann/ordo/esvapi"
	"bitbucket.org/smussmann/ordo/lectionary"

	"context"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"path/filepath"
	"regexp"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/vjeantet/eastertime"
)

const redirectTemplate = `
<html><head>
<noscript>
<meta http-equiv="refresh" content="0; url={{.}}">
</noscript>
<script>
var now = new Date();
var today_url = "/" + now.getFullYear() + 
                "/" + ("0" + (now.getMonth() + 1)).slice(-2) +
                "/" + ("0" + now.getDate()).slice(-2);
window.location = today_url;
</script>
</head>
</html>
`

func MakeReading(ctx context.Context, token string, day time.Time) func(lectionary.Lectionary) template.HTML {
	return func(l lectionary.Lectionary) template.HTML {
		passage_reference := l.GetReading(day)
		passage, err := esvapi.Get(ctx, token, passage_reference)
		if err != nil {
			log.Printf("Failed to fetch reading with error: %s", err)
			return template.HTML(passage_reference)
		}
		return template.HTML(passage)
	}
}

func utcEaster(year int) time.Time {
	easter, _ := eastertime.CatholicByYear(year)
	return time.Date(easter.Year(), easter.Month(), easter.Day(), 0, 0, 0, 0, time.UTC)
}

type Config struct {
	PageTemplates   map[string]string
	ServiceTemplate string
	// These paths, if specified, will result in a redirect to today's service if visited.
	// If not specified, the default is "/today".
	RedirectPaths []string
	ESVToken      string
}

func (cfg Config) SetFuncMap(ctx context.Context, t *template.Template, day time.Time) *template.Template {
	funcMap := make(template.FuncMap)
	for k, v := range lectionary.LectionaryMap {
		funcMap[k] = v
	}
	funcMap["reading"] = MakeReading(ctx, cfg.ESVToken, day)
	funcMap["antiphon"] = func() template.HTML {
		return template.HTML(antiphons.GetAntiphon(day))
	}
	funcMap["daysUntilEaster"] = func() int {
		// The only error that gets returned is if year is < 0, which it won't be.
		easter := utcEaster(day.Year())
		if !easter.After(day) {
			easter = utcEaster(day.Year() + 1)
		}
		return int(easter.Sub(day) / (24 * time.Hour))
	}
	return t.Funcs(funcMap)
}

func AddToGroup(config Config, group *gin.RouterGroup) {
	var redirectPaths []string
	if len(config.RedirectPaths) == 0 {
		redirectPaths = []string{"/today"}
	} else {
		redirectPaths = config.RedirectPaths
	}
	for _, path := range redirectPaths {
		group.GET(path, func(c *gin.Context) {
			template.Must(template.New("redirect").Parse(redirectTemplate)).Execute(c.Writer, time.Now().Format("/2006/01/02"))
			return
		})
	}

	for path, templatePath := range config.PageTemplates {
		group.GET(path, func(c *gin.Context) {
			err := template.Must(template.ParseFiles(templatePath)).Execute(c.Writer, nil)
			if err != nil {
				log.Printf("Error serving %q: %v", c.Request.URL.EscapedPath(), err)
			}
			return
		})
	}
	group.GET("/:year/:month/:day", func(c *gin.Context) {
		date := fmt.Sprintf("%s/%s/%s", c.Param("year"), c.Param("month"), c.Param("day"))
		t, err := time.Parse("2006/01/02", date)
		if err != nil {
			log.Printf("Error parsing %q as date: %s", date, err)
			http.NotFound(c.Writer, c.Request)
			return
		}
		tmpl := template.Must(config.SetFuncMap(c.Request.Context(), template.New("main"), t).ParseFiles(config.ServiceTemplate))
		err = tmpl.ExecuteTemplate(c.Writer, filepath.Base(config.ServiceTemplate), t)
		if err != nil {
			log.Printf("Error serving service page: %v", err)
		}
	})
}

func GetServeMux(config Config) *http.ServeMux {
	mux := http.NewServeMux()
	if len(config.RedirectPaths) == 0 {
		mux.HandleFunc("/today", func(w http.ResponseWriter, r *http.Request) {
			template.Must(template.New("redirect").Parse(redirectTemplate)).Execute(w, time.Now().Format("/2006/01/02"))
			return
		})
	}
	mux.HandleFunc("/", makeHandler(config))
	return mux
}

var pathRegexp = regexp.MustCompile("/[0-9]{4}/[01][0-9]/[0-3][0-9]")

func makeHandler(config Config) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		for _, path := range config.RedirectPaths {
			if path == r.URL.EscapedPath() {
				template.Must(template.New("redirect").Parse(redirectTemplate)).Execute(w, time.Now().Format("/2006/01/02"))
				return
			}
		}
		if templatePath, ok := config.PageTemplates[r.URL.EscapedPath()]; ok {
			err := template.Must(template.ParseFiles(templatePath)).Execute(w, nil)
			if err != nil {
				log.Printf("Error serving %q: %v", r.URL.EscapedPath(), err)
			}
			return
		}
		if !pathRegexp.MatchString(r.URL.EscapedPath()) {
			http.NotFound(w, r)
			return
		}
		t, err := time.Parse("/2006/01/02", r.URL.EscapedPath())
		if err != nil {
			panic(err)
		}
		tmpl := template.Must(config.SetFuncMap(r.Context(), template.New("main"), t).ParseFiles(config.ServiceTemplate))
		err = tmpl.ExecuteTemplate(w, filepath.Base(config.ServiceTemplate), t)
		if err != nil {
			log.Printf("Error serving service page: %v", err)
		}
	}
}
