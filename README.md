# Ordo

The purpose of Ordo is to add lectionaries to golang templates to enable easy (or at least easier) writing of orders of service, particularly for prayer offices like Matins.

## Design

Ordo is designed so that the definition of the order of service, including referencing the lectionary, occurs entirely within the template file.

The `appengine` directory includes an example template, which you can see in action at https://bible-pit-prayer.appspot.com.
