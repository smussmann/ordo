package esvapi

//   Copyright 2016 Sam Mussmann
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

const urlFormat = "https://api.esv.org/v3/passage/html/?q=%s&include-footnotes=false&include-headings=false&include-audio-link=false&include-chapter-numbers=false&include-surrounding-chapters-below=false"

type esvV3Response struct {
	Passages []string
}

func Get(ctx context.Context, token string, passage string) (string, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf(urlFormat, url.QueryEscape(passage)), http.NoBody)
	if err != nil {
		return "", err
	}
	req.Header.Set("Authorization", fmt.Sprintf("Token %s", token))
	resp, err := (&http.Client{}).Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	var b bytes.Buffer
	b.ReadFrom(resp.Body)
	if b.String() == "ERROR: No results were found for your search." {
		return "", fmt.Errorf("No results returned for search: %s", passage)
	}
	if resp.StatusCode != 200 {
		return "", fmt.Errorf("Got HTTP status %q: %q", resp.Status, b.String())
	}
	var jsonResponse esvV3Response
	err = json.Unmarshal(b.Bytes(), &jsonResponse)
	if err != nil {
		return "", fmt.Errorf("Error parsing JSON response: %q")
	}
	return strings.Join(jsonResponse.Passages, "\n"), nil
}
