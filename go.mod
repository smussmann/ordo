module bitbucket.org/smussmann/ordo

go 1.14

require (
	github.com/gin-gonic/gin v1.5.0
	github.com/vjeantet/eastertime v0.0.0-20160228130558-3941bbcf8209
)
