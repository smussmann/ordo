package antiphons

import (
	"fmt"
	"time"
)

func makeAntiphon(firstLine, secondLine string) string {
	return fmt.Sprintf("<span class=antiphon><span class=first-line>%s</span><br><span class=second-line>%s</span></span>",
		firstLine, secondLine)
}

var greatOAntiphons = map[int]string{
	17: makeAntiphon("O Wisdom, proceeding from the mouth of the Most High, pervading and permeating all creation, mightily ordering all things:",
		"Come and teach us the way of prudence"),
	18: makeAntiphon("O Adonai, and ruler of the house of Israel, who appeared to Moses in the burning bush and gave him the Law on Sinai:",
		"Come with outstretched arm and redeem us."),
	19: makeAntiphon("O Root of Jesse, standing as an ensign before the peoples, before whom all kings are mute, to whom the nations will do homage:",
		"Come quickly to deliver us."),
	20: makeAntiphon("O Key of David and scepter of the house of Israel, You open and no one can close, You close and no one can open:",
		"Come and rescue the prisoners who are in darkness and in the shadow of death."),
	21: makeAntiphon("O Dayspring, splendor of light everlasting:",
		"Come and enlighten those who sit in darkness and in the shadow of death."),
	22: makeAntiphon("O King of the nations, the ruler they long for, the cornerstone uniting all people:",
		"Come and save us all, whom You formed out of clay."),
	23: makeAntiphon("O Emmanuel, our King and our Lord, the anointed for the nations and their Savior:",
		"Come and save us, O Lord our God."),
}

func GetAntiphon(t time.Time) string {
	t = time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.UTC)
	if antiphon, ok := greatOAntiphons[t.Day()]; t.Month() == time.December && ok {
		return antiphon
	}
	return ""
}
