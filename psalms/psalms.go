package main

//   Copyright 2016 Sam Mussmann
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import (
	"fmt"
	"sort"
	"strings"
)

var psalmsByVerseCount = map[string]int{
	"Psalm   1": 6,
	"Psalm   2": 12,
	"Psalm   3": 8,
	"Psalm   4": 8,
	"Psalm   7": 17,
	"Psalm   8": 9,
	"Psalm   9": 20,
	"Psalm  10": 18,
	"Psalm  13": 6,
	"Psalm  14": 7,
	"Psalm  15": 5,
	"Psalm  16": 11,
	"Psalm  17": 15,
	"Psalm  18": 50,
	"Psalm  19": 14,
	"Psalm  20": 9,
	"Psalm  21": 13,
	"Psalm  22": 31,
	"Psalm  23": 6,
	"Psalm  24": 10,
	"Psalm  25": 22,
	"Psalm  26": 12,
	"Psalm  27": 14,
	"Psalm  28": 9,
	"Psalm  29": 11,
	"Psalm  30": 12,
	"Psalm  31": 24,
	"Psalm  32": 11,
	"Psalm  33": 22,
	"Psalm  34": 22,
	"Psalm  36": 12,
	"Psalm  38": 22,
	"Psalm  39": 13,
	"Psalm  41": 13,
	"Psalm  42": 11,
	"Psalm  43": 5,
	"Psalm  44": 26,
	"Psalm  45": 17,
	"Psalm  46": 11,
	"Psalm  47": 9,
	"Psalm  48": 14,
	"Psalm  49": 20,
	"Psalm  50": 23,
	"Psalm  51": 19,
	"Psalm  53": 6,
	"Psalm  55": 23,
	"Psalm  57": 11,
	"Psalm  59": 17,
	"Psalm  60": 12,
	"Psalm  61": 8,
	"Psalm  62": 12,
	"Psalm  63": 11,
	"Psalm  64": 10,
	"Psalm  65": 13,
	"Psalm  66": 20,
	"Psalm  67": 7,
	"Psalm  68": 35,
	"Psalm  70": 5,
	"Psalm  71": 24,
	"Psalm  72": 20,
	"Psalm  73": 28,
	"Psalm  74": 23,
	"Psalm  75": 10,
	"Psalm  76": 12,
	"Psalm  77": 20,
	"Psalm  78": 72,
	"Psalm  80": 19,
	"Psalm  81": 16,
	"Psalm  82": 8,
	"Psalm  84": 12,
	"Psalm  85": 13,
	"Psalm  86": 17,
	"Psalm  87": 7,
	"Psalm  88": 18,
	"Psalm  89": 52,
	"Psalm  90": 17,
	"Psalm  91": 16,
	"Psalm  92": 15,
	"Psalm  93": 5,
	"Psalm  94": 23,
	"Psalm  95": 11,
	"Psalm  96": 13,
	"Psalm  97": 12,
	"Psalm  98": 9,
	"Psalm  99": 9,
	"Psalm 100": 5,
	"Psalm 101": 8,
	"Psalm 102": 28,
	"Psalm 103": 22,
	"Psalm 104": 35,
	"Psalm 105": 45,
	"Psalm 106": 48,
	"Psalm 107": 43,
	"Psalm 108": 13,
	"Psalm 110": 7,
	"Psalm 111": 10,
	"Psalm 112": 10,
	"Psalm 113": 9,
	"Psalm 114": 8,
	"Psalm 115": 18,
	"Psalm 116": 19,
	"Psalm 117": 2,
	"Psalm 118": 29,
	"Psalm 119": 176,
	"Psalm 120": 7,
	"Psalm 121": 8,
	"Psalm 122": 9,
	"Psalm 123": 4,
	"Psalm 124": 8,
	"Psalm 125": 5,
	"Psalm 126": 6,
	"Psalm 127": 5,
	"Psalm 128": 6,
	"Psalm 129": 8,
	"Psalm 130": 8,
	"Psalm 131": 3,
	"Psalm 132": 18,
	"Psalm 133": 3,
	"Psalm 134": 3,
	"Psalm 135": 21,
	"Psalm 136": 26,
	"Psalm 138": 8,
	"Psalm 140": 13,
	"Psalm 141": 10,
	"Psalm 142": 7,
	"Psalm 144": 15,
	"Psalm 145": 21,
	"Psalm 146": 10,
	"Psalm 147": 20,
	"Psalm 148": 14,
	"Psalm 149": 9,
	"Psalm 150": 6,
}

func FindPsalmInRange(target_min, target_max int, blacklist map[string]bool) (string, int, error) {
	for psalm, length := range psalmsByVerseCount {
		if _, exists := blacklist[psalm]; !exists && target_min <= length && target_max >= length {
			return psalm, length, nil
		}
	}
	return "", 0, fmt.Errorf("No psalm in target range.")
}

func MakeAReading(target_min, target_max int, blacklist map[string]bool) (string, error) {
	psalms := make([]string, 0)
	length := 0
	for length < target_min {
		next_psalm, next_length, err := FindPsalmInRange((target_min-length)/2, target_max-length, blacklist)
		if err != nil {
			return "", err
		}
		psalms = append(psalms, next_psalm)
		length += next_length
		blacklist[next_psalm] = true
	}
	sort.Strings(psalms)
	return strings.Join(psalms, "; "), nil
}

func MakeLectionary(target_min, target_max int) []string {
	readings := make([]string, 0)
	blacklist := make(map[string]bool)
	for {
		reading, err := MakeAReading(target_min, target_max, blacklist)
		if err != nil {
			break
		}
		readings = append(readings, reading)
	}
	return readings
}

func main() {
	longest_readings := make([]string, 0)
	index_of_longest := 0
	for i := 0; i < 10000; i++ {
		readings := MakeLectionary(34, 40)
		if len(readings) > len(longest_readings) {
			longest_readings = readings
			index_of_longest = i
		}
	}
	fmt.Println(len(longest_readings))
	fmt.Println(index_of_longest)
	for _, reading := range longest_readings {
		fmt.Println(reading)
	}
}
